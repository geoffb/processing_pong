import processing.serial.*;
import processing.sound.*;

// Arduino
Serial entryPort;
String arduino; 
String arduinoNumber;

//Bat 
int batWidht = 200;
int batHeight = 20;

// Window
int windowWidth = 1023 + batWidht; 
int windowHeight = 500; 

//Ball variables
int ballRay = 10;
boolean ballMoving = false;
boolean ballHorizontalDirection = true; // true = going right 
boolean ballVerticalDirection = true; // true = going down 
int ballXCoordinate = ballRay;
int ballYCoordinate = ballRay;
int speed = 7;

// Score 
int score = 0;

// Sounds
SoundFile loseSound;
SoundFile ticSound;
SoundFile borderSound;

void setup() {
  arduino = Serial.list()[7]; // Switch COM port if needed
  entryPort = new Serial (this, arduino, 9600);

  // Window
  size(1223, 500);

  //Initial Drawing
  drawInterface(0, ballXCoordinate, ballYCoordinate, score);

  // Load sounds
  loseSound = new SoundFile(this, "lose.mp3");
  borderSound = new SoundFile(this, "border.mp3");
  ticSound = new SoundFile(this, "tic.mp3");
}

void draw() {
  if (entryPort.available() > 0) {  
    arduinoNumber = entryPort.readStringUntil('\n');

    // Check if arduino number is not equal to null and ball is moving
    if (arduinoNumber != null && ballMoving){ 
      arduinoNumber = arduinoNumber.trim();
      int intArduinoValue = Integer.parseInt(arduinoNumber);

      // Ball
      //Y
      if (ballVerticalDirection == true) {
        ballYCoordinate = ballYCoordinate + speed; 
      }
      else {
        ballYCoordinate = ballYCoordinate - speed; 
      }

      //X
      if (ballHorizontalDirection == true) {
        ballXCoordinate = ballXCoordinate + speed; 
      }
      else {
        ballXCoordinate = ballXCoordinate - speed; 
      }

      // Bottom Border
      if (ballYCoordinate > windowHeight - ballRay) {
        // Restart game
        loseSound.play();

        ballXCoordinate = ballRay;
        ballYCoordinate = ballRay;

        score = 0;
        ballMoving = false;
      }

      // Top Border
      if (ballYCoordinate < ballRay) {
        ballVerticalDirection = true;
        borderSound.play();
      } 

      // Right Border
      if (ballXCoordinate > windowWidth - ballRay) {
        ballHorizontalDirection = false;
        borderSound.play();
      }

      // Left Border
      if (ballXCoordinate < ballRay) {
        ballHorizontalDirection = true;
        borderSound.play();
      } 

      // Collision Bat/Ball
      if ((ballXCoordinate + ballRay) >= intArduinoValue) {
        // X Collision
        if ((ballXCoordinate + ballRay) <= (intArduinoValue + batWidht)) {
          // Y Collision
          if ((ballYCoordinate + ballRay) > (windowHeight - batHeight)) {
            ticSound.play();
            ballVerticalDirection = false;
            score = score + 1;
          }
        }
      }

      drawInterface(intArduinoValue, ballXCoordinate, ballYCoordinate, score);
    }
  }
}

void drawInterface(int arduinoNumber, int xBall, int yBall, int scoreKeeper){
  // background
  background(0, 0, 0);

  // use white
  fill(255, 255, 255);

  // Bat
  rect(arduinoNumber, windowHeight - batHeight, batWidht, batHeight); // BatXcoordinate, BatYc, Width, Height

  // Ball
  ellipse (xBall, yBall, ballRay*2, ballRay*2);

  // Text Size
  textSize(25);
  text(scoreKeeper, windowWidth/2, 25);
}

void keyPressed() {
  if (keyCode == 32) {
    ballMoving = true;
  }
}