#define LED 13
#define BUTTON 2
int val = 0;

void setup() {
  pinMode(BUTTON, INPUT);
  pinMode(LED, OUTPUT);
  digitalWrite (BUTTON, HIGH);
  
  Serial.begin(9600);
}

void loop() {
  val = digitalRead(BUTTON);
  digitalWrite(LED, val);

  Serial.println(val);
}
